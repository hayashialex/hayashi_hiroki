package board.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import board.beans.User;

@WebFilter("/*")
public class LoginFilter implements Filter {
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

		HttpSession session = ((HttpServletRequest)request).getSession();
		User user = (User) session.getAttribute("loginUser");
		String requestUri = ((HttpServletRequest) request).getRequestURI();

		if(!(requestUri.contains("login") || requestUri.contains("css"))){
			if(user != null){
				chain.doFilter(request, response);
			}else{
				List<String> messages = new ArrayList<String>();
				messages.add("ログインされていません");
				session.setAttribute("errorMessages", messages);
				((HttpServletResponse)response).sendRedirect("login");
			}
		}else {
			chain.doFilter(request, response);
		}
	}
	public LoginFilter() {
	}
	public void destroy() {
	}
	public void init(FilterConfig fConfig) throws ServletException {

	}
}
