package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import board.beans.UserComment;
import board.dao.UserCommentDao;

public class UserCommentService {

    public List<UserComment> getComment() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCommentDao commentDao = new UserCommentDao();
            List<UserComment> ret = commentDao.getComment(connection);

            commit(connection);
            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}