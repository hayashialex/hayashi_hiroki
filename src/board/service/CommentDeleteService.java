package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.beans.User;
import board.dao.CommentDeleteDao;


public class CommentDeleteService {
    public User register(int commentId) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDeleteDao  commentDeleteDao = new CommentDeleteDao();
            commentDeleteDao.delete(connection, commentId);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return null;
    }
}