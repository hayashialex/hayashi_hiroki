package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.dao.CheckParaDao;

public class CheckParaService {

    public static Boolean CheckPara(int intCheckParaId) {

        Connection connection = null;
        try {
            connection = getConnection();

            CheckParaDao checkParaDao = new CheckParaDao();
            Boolean bool = checkParaDao.CheckPara(connection, intCheckParaId);

            commit(connection);
            return bool;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
