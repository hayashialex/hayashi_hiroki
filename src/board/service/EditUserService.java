package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.dao.EditUserDao;
import board.utils.CipherUtil;

public class EditUserService {
    public User getUserId(int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            EditUserDao  editUserDao = new EditUserDao();
            User editUser = editUserDao.getUserId(connection, userId);

            commit(connection);
            return editUser;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
	public void userUpdate(User user, int userId) {

		Connection connection = null;
		try {
			connection = getConnection();

			if(!StringUtils.isBlank(user.getPassword())){
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);
			}else{
				user.setPassword(user.getPassword());
			}
			EditUserDao editUserDao = new EditUserDao();
			editUserDao.userUpdate(connection, user, userId);

			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
