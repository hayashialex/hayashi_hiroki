package board.service;



import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import board.beans.UserPost;
import board.dao.UserPostDao;

public class UserPostService {
	private static final int LIMIT_NUM = 1000;
	public List<UserPost> getUserPosts(String category, String startDate, String endDate) {

	    Connection connection = null;
	    try {
	        connection = getConnection();


	        UserPostDao userPostDao = new UserPostDao();
	        List<UserPost> ret = userPostDao.getUserPosts(connection, LIMIT_NUM, category, startDate, endDate);

	        commit(connection);

	        return ret;
	    } catch (RuntimeException e) {
	        rollback(connection);
	        throw e;
	    } catch (Error e) {
	        rollback(connection);
	        throw e;
	    } finally {
	        close(connection);
	    }
	}
}
