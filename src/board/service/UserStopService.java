package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.beans.User;
import board.dao.UserStopDao;


public class UserStopService {
    public User register(UserStopService userStopService, int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserStopDao  userStopDao = new UserStopDao();
            userStopDao.update(connection, userStopService, userId);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return null;
    }
}