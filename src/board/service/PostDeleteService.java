package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.beans.User;
import board.dao.PostDeleteDao;


public class PostDeleteService {
    public User register(int postId) {

        Connection connection = null;
        try {
            connection = getConnection();

            PostDeleteDao  PostDeleteDao = new PostDeleteDao();
            PostDeleteDao.delete(connection, postId);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return null;
    }
}