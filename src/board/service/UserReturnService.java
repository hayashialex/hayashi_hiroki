package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.beans.User;
import board.dao.UserReturnDao;


public class UserReturnService {
    public User register(UserReturnService userReturnService, int userId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserReturnDao  userReturnDao = new UserReturnDao();
            userReturnDao.update(connection, userReturnService, userId);


            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
		return null;
    }
}