package board.service;

import static board.utils.CloseableUtil.*;
import static board.utils.DBUtil.*;

import java.sql.Connection;

import board.dao.UserCheckDao;

public class UserCheckService {

    public static Boolean userCheck(String loginId) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserCheckDao userCheckDao = new UserCheckDao();
            Boolean bool = userCheckDao.userCheck(connection, loginId);

            commit(connection);
            return bool;

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }
}
