package board.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.service.UserReturnService;

@WebServlet("/userReturn")
public class UserReturnServlet extends HttpServlet {
  private static final long serialVersionUID = 1L;

  @Override
  protected void doPost(HttpServletRequest request,
      HttpServletResponse response)
      throws ServletException, IOException {

    int userId = Integer.parseInt(request.getParameter("ReturnUserId"));

    UserReturnService userReturnService = new UserReturnService();
    userReturnService.register(userReturnService, userId);

    String url = "Member";
    response.sendRedirect(url);
  }
  }

