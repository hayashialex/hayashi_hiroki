package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Message;
import board.beans.User;
import board.service.MessageService;

@WebServlet(urlPatterns = { "/newMessage" })
public class NewPostServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {


        request.getRequestDispatcher("newPost.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();
		if (isValid(request, messages) == true) {

			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();
			message.setCategories(request.getParameter("category"));
			message.setSubject(request.getParameter("subject"));
			message.setMainPost(request.getParameter("mainPost"));
			message.setUser_id(user.getId());

			new MessageService().register(message);

			response.sendRedirect("./");
		} else {

			String mainPost = request.getParameter("mainPost");
			String subject = request.getParameter("subject");
			String category = request.getParameter("category");

			System.out.println(mainPost);

			session.setAttribute("mainPost", mainPost);
			session.setAttribute("subject", subject);
			session.setAttribute("category", category);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newMessage");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String mainPost = request.getParameter("mainPost");
		String subject = request.getParameter("subject");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(subject) == true) {
			messages.add("件名を入力してださい");
		}
		if (StringUtils.isEmpty(mainPost) == true) {
			messages.add("本文を入力してください");
		}
		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリを入力してださい");
		}
		if (30 < subject.length()) {
			messages.add("30文字以下で入力してください");
		}
		if (1000 < mainPost.length()) {
			messages.add("1000文字以下で入力してください");
		}
		if (10 < category.length()) {
			messages.add("10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}