package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Comment;
import board.beans.User;
import board.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");

			Comment comment = new Comment();
			comment.setMain_commet((request.getParameter("mainComment")));
			comment.setUserId(user.getId());
			comment.setPostId(Integer.parseInt(request.getParameter("postId")));

			new CommentService().register(comment);

			response.sendRedirect("./");
		} else {
			String mainComment = request.getParameter("mainComment");
			int postIdNumber = Integer.parseInt(request.getParameter("postId"));

			session.setAttribute("mainComment", mainComment);
			session.setAttribute("postIdNumber", postIdNumber);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("./");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String mainComment = request.getParameter("mainComment");

		if (StringUtils.isBlank(mainComment) == true) {
			messages.add("メッセージを入力してください");
		}
		if (500 < mainComment.length()) {
			messages.add("500文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}