package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Position;
import board.beans.User;
import board.service.BranchService;
import board.service.PositionService;
import board.service.UserCheckService;
import board.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("branch", branch);
		request.setAttribute("position", position);


		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isValid(request, messages) == true) {

			User user = new User();
			user.setLoginId(request.getParameter("roginId"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch(Integer.parseInt(request.getParameter("branch")));
			user.setPosition(Integer.parseInt(request.getParameter("position")));

			String loginId = request.getParameter("roginId");

			new UserCheckService();
			UserCheckService.userCheck(loginId);

			new UserService().register(user);
			response.sendRedirect("Member");

		} else {
			String name = request.getParameter("name");
			String loginId = request.getParameter("roginId");

			int branchId = Integer.parseInt(request.getParameter("branch"));
			int positionId = Integer.parseInt(request.getParameter("position"));

			session.setAttribute("branchId", branchId);
			session.setAttribute("positionId", positionId);


			session.setAttribute("name", name);
			session.setAttribute("roginId", loginId);

			session.setAttribute("errorMessages", messages);

			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String loginId = request.getParameter("roginId");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String secondPassword = request.getParameter("passwordSecond");
		int branchId = Integer.parseInt(request.getParameter("branch"));
		int positionId = Integer.parseInt(request.getParameter("position"));

		Boolean bool = UserCheckService.userCheck(loginId);

		if (StringUtils.isEmpty(loginId) == true) {
			messages.add("ログインIDを入力してください");
		}else if(loginId.matches("[ -~]") || 6 > loginId.length() || 20 < loginId.length()) {
			messages.add("ログインIDの定義に従っていません");
		}
		if(bool == false){
		messages.add("ログインIDが他のユーザーと重複しています");
		 }
		if (StringUtils.isEmpty(name) == true) {
			messages.add("名前を入力してください");
		}
		if(StringUtils.isEmpty(password) == true){
			messages.add("パスワードを入力してください");
		}else if(!password.equals(secondPassword)) {
			messages.add("パスワードが一致しません");
		}else if(password.matches("[^ -~｡-ﾟ]") || 6 > password.length() || 20 < password.length()) {
			messages.add("パスワードの定義に従っていません");
		}
		if(branchId != 1 && positionId <= 2) {
			messages.add("支店と部署・役職名の組み合わせが不正です");
		}
		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}





