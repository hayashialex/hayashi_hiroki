package board.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import board.beans.Branch;
import board.beans.Position;
import board.beans.User;
import board.service.BranchService;
import board.service.CheckParaService;
import board.service.EditUserService;
import board.service.PositionService;
import board.service.UserCheckService;

@WebServlet(urlPatterns = { "/editUserSevlet" })
public class EditUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if (isGetValid(request, messages) == true) {

		int userId = Integer.parseInt(request.getParameter("EditUserId"));

		User editUser = new EditUserService().getUserId(userId);
		request.setAttribute("editUser", editUser);


		List<Branch> branch = new BranchService().getBranch();
		List<Position> position = new PositionService().getPosition();

		request.setAttribute("branch", branch);
		request.setAttribute("position", position);

		request.getRequestDispatcher("editUser.jsp").forward(request, response);
		}else{
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("Member");
		}
	}
	private boolean isGetValid(HttpServletRequest request, List<String> messages) {

		String checkParaId = request.getParameter("EditUserId");

		System.out.println(checkParaId);

		if(checkParaId == null) {
			messages.add("不正なパラメーターです");
		}else if(!checkParaId.matches("[0-9]")) {
			messages.add("不正なパラメーターです");
		}else {
			int intCheckParaId = Integer.parseInt(checkParaId);
			CheckParaService.CheckPara(intCheckParaId);
		}
		Boolean bool = CheckParaService.CheckPara(0);
		if(bool == true) {
			messages.add("不正なパラメーターです");
		}
			if (messages.size() == 0) {
				return true;
			} else {
				return false;
			}
		}
		@Override
		protected void doPost(HttpServletRequest request,
				HttpServletResponse response) throws ServletException, IOException {

			List<String> messages = new ArrayList<String>();
			HttpSession session = request.getSession();


			if (isValid(request, messages) == true) {

				int userId = Integer.parseInt(request.getParameter("UserUpdateId"));
				User user = new User();
				user.setName(request.getParameter("name"));
				user.setPassword(request.getParameter("password"));
				user.setBranch(Integer.parseInt(request.getParameter("branch")));
				user.setPosition(Integer.parseInt(request.getParameter("position")));
				user.setLoginId(request.getParameter("loginId"));

				EditUserService editUserService = new EditUserService();
				editUserService.userUpdate(user, userId);

				response.sendRedirect("Member");

			} else {
				List<Branch> branch = new BranchService().getBranch();
				List<Position> position = new PositionService().getPosition();

				User editUser = new User();
				editUser.setName(request.getParameter("name"));
				editUser.setLoginId(request.getParameter("loginId"));
				editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
				editUser.setPosition(Integer.parseInt(request.getParameter("position")));

				request.setAttribute("editUser", editUser);
				request.setAttribute("branch", branch);
				request.setAttribute("position", position);

				session.setAttribute("errorMessages", messages);
				request.getRequestDispatcher("editUser.jsp").forward(request, response);
			}
		}

		private boolean isValid(HttpServletRequest request, List<String> messages) {
			String name = request.getParameter("name");
			String loginId = request.getParameter("loginId");
			String defaultLoginId = request.getParameter("defaultUserId");
			String password = request.getParameter("password");
			String secondPassword = request.getParameter("passwordSecond");
			int branchId = Integer.parseInt(request.getParameter("branch"));
			int positionId = Integer.parseInt(request.getParameter("position"));

			Boolean bool = UserCheckService.userCheck(loginId);

			if (StringUtils.isBlank(loginId) == true) {
				messages.add("ログインIDを入力してください");
			}else if(loginId.matches("[ -~]") && 6 <= loginId.length() && 20 >= loginId.length()) {
				messages.add("定義に従っていません1");
			}
			if(!loginId.equals(defaultLoginId)) {
				if(bool == false){
					messages.add("ログインIDが他のユーザーと重複しています");
			}
			if (StringUtils.isBlank(name) == true) {
				messages.add("名前を入力してください");
			}
			}
			if(StringUtils.isBlank(password) != true && StringUtils.isBlank(secondPassword) != true) {
			if(!password.equals(secondPassword)) {
				messages.add("パスワードが一致しません");
			}else if(password.matches("[^ -~｡-ﾟ]")) {
				messages.add("定義に従っていません");
			}else if(6 > password.length()) {
				messages.add("定義に従っていません");
			}else if(20 < password.length()) {
				messages.add("定義に従っていません");
			}
			}
			if(branchId != 1 && positionId <= 2) {
				messages.add("不正な組み合わせです");
			}
			if (messages.size() == 0) {
				return true;
			} else {
				return false;
			}
		}
	}


