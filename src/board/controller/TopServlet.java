package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.User;
import board.beans.UserComment;
import board.beans.UserPost;
import board.service.UserCommentService;
import board.service.UserPostService;

@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        User user = (User) request.getSession().getAttribute("loginUser");
        boolean isShowMessageForm;
        if (user != null) {
            isShowMessageForm = true;
        } else {
            isShowMessageForm = false;
        }

        String category = request.getParameter("category");
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");

    	List<UserComment> comments = new UserCommentService().getComment();
        request.setAttribute("comments", comments);

        List<UserPost> posts = new UserPostService().getUserPosts(category, startDate, endDate);
        request.setAttribute("posts", posts);
        request.setAttribute("isShowMessageForm", isShowMessageForm);
        request.setAttribute("category", category);
        request.setAttribute("startDate", startDate);
        request.setAttribute("endDate", endDate);

        request.getRequestDispatcher("top.jsp").forward(request, response);
    }
}