package board.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import board.beans.Member;
import board.service.MessageService;

@WebServlet(urlPatterns = {"/Member"})
public class MemberServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<Member> members = new MessageService().getMember();

        request.setAttribute("members", members);

        request.getRequestDispatcher("userManagement.jsp").forward(request, response);
    }
    protected void doPost(HttpServletRequest request,
    	      HttpServletResponse response)
    	      throws ServletException, IOException {

    	    String message = request.getParameter("stop");
    	    System.out.println(message);

    	    request.setAttribute("message", message);

    	    RequestDispatcher dispatcher = request.getRequestDispatcher("userManagement.jsp");
    	    dispatcher.forward(request, response);
    	 }
}
