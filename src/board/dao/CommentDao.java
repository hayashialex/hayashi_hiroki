package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.beans.Comment;
import board.exception.SQLRuntimeException;


public class CommentDao {

	public void insert(Connection connection, Comment comment) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO comments ( ");
			sql.append("main_comment");
			sql.append(", user_id");
			sql.append(", message_id");
			sql.append(", created_date");
			sql.append(") values ( ");
			sql.append("?"); //main_comment
			sql.append(", ?"); //user_id
			sql.append(", ?"); // message_id
			sql.append(", CURRENT_TIMESTAMP"); //
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, comment.getMain_commet());
			ps.setInt(2, comment.getUserId());
			ps.setInt(3, comment.getPostId());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}