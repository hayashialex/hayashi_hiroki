package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.commons.lang.StringUtils;

import board.beans.User;
import board.exception.SQLRuntimeException;


public class EditUserDao {
	public User getUserId(Connection connection, int userId) {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE id = ?";

			ps = connection.prepareStatement(sql);
			ps.setInt(1, userId);

			ResultSet rs = ps.executeQuery();
			User ret = getEdit(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private User getEdit(ResultSet rs) throws SQLException {

		try{
			if(rs.next()){
				int id = rs.getInt("id");
				String name = rs.getString("name");
				int branch = rs.getInt("branch");
				int position = rs.getInt("position");
				String loginId = rs.getString("login_id");

				User user = new User();
				user.setId(id);
				user.setName(name);
				user.setBranch(branch);
				user.setPosition(position);
				user.setLoginId(loginId);

				return user;
			}
			return null;
		}finally{
			close(rs);
		}
	}
	public User userUpdate(Connection connection, User user, int userId){

		PreparedStatement ps = null;
		try {

			StringBuilder sql = new StringBuilder();
			sql.append("update users set");
			sql.append(" name = ?");
			if(!StringUtils.isBlank(user.getPassword())){
				sql.append(", password = ?");
			}
			sql.append(", branch = ?");
			sql.append(", position = ?");
			sql.append(", login_id = ?");
			sql.append(", created_date = CURRENT_TIMESTAMP");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" where");
			sql.append(" id ="+userId);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getName());
			if(!StringUtils.isBlank(user.getPassword())){
			ps.setString(2, user.getPassword());
			ps.setInt(3, user.getBranch());
			ps.setInt(4, user.getPosition());
			ps.setString(5, user.getLoginId());
			ps.executeUpdate();
			}else{
				ps.setInt(2, user.getBranch());
				ps.setInt(3, user.getPosition());
				ps.setString(4, user.getLoginId());
				ps.executeUpdate();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		return null;
	}
}