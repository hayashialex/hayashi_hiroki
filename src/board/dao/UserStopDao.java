package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.exception.SQLRuntimeException;
import board.service.UserStopService;

public class UserStopDao {
	public void update(Connection connection, UserStopService userStopService, int userId) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("update users set");
			sql.append(" is_stoped=1");
			sql.append(" where");
			sql.append(" id="+userId);


			ps = connection.prepareStatement(sql.toString());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}