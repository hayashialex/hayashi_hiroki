package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import board.beans.UserComment;
import board.exception.SQLRuntimeException;

public class UserCommentDao {
	public List<UserComment> getComment(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select comments.id as id");
			sql.append(", comments.main_comment as main_comment");
			sql.append(", comments.created_date as created_date");
			sql.append(", comments.user_id as user_id");
			sql.append(", comments.message_id as message_id");
			sql.append(", users.name as name");
			sql.append(" from");
			sql.append(" users");
			sql.append(" inner join");
			sql.append(" comments");
			sql.append(" on");
			sql.append(" comments.user_id = users.id;");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<UserComment> ret = toCommentList(rs);

			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserComment> toCommentList(ResultSet rs)
			throws SQLException {

		List<UserComment> ret = new ArrayList<UserComment>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String mainComment = rs.getString("main_comment");
				String createdDate = rs.getString("created_date");
				int userId = rs.getInt("user_id");
				int messageId = rs.getInt("message_id");
				String name = rs.getString("name");

				UserComment userComment = new UserComment();
				userComment.setId(id);
				userComment.setMain_comment(mainComment);
				userComment.setCreatedDate(createdDate);
				userComment.setUserId(userId);
				userComment.setMessageId(messageId);
				userComment.setName(name);

				ret.add(userComment);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

}