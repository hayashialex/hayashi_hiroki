package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.beans.Message;
import board.exception.SQLRuntimeException;

public class MessageDao {

    public void insert(Connection connection, Message message) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("insert into posts ( ");
            sql.append("user_id");
            sql.append(", subject");
            sql.append(", categories");
            sql.append(", main_post");
            sql.append(", created_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?"); //subject
            sql.append(", ?"); //categories
            sql.append(", ?"); // mainpost
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            ps.setInt(1, message.getUser_id());
            ps.setString(2, message.getSubject());
            ps.setString(3, message.getCategories());
            ps.setString(4, message.getMainPost());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
}