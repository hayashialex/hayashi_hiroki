package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import board.beans.UserPost;
import board.exception.SQLRuntimeException;

public class UserPostDao {

	public List<UserPost> getUserPosts(Connection connection, int num, String category, String startDate, String endDate) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("select ");
			sql.append("posts.id as id, ");
			sql.append("posts.subject as subject, ");
			sql.append("posts.main_post as main_post, ");
			sql.append("posts.categories as category, ");
			sql.append("posts.created_date as created_date, ");
			sql.append("posts.user_id as user_id, ");
			sql.append("users.name as name ");
			sql.append("FROM ");
			sql.append("posts ");
			sql.append("INNER JOIN ");
			sql.append("users ");
			sql.append("ON posts.user_id = users.id ");
			sql.append("where posts.created_date >= ? ");
			sql.append("and posts.created_date <= ? ");
			if(!(StringUtils.isEmpty(category))) {
				sql.append("and categories like ? ");
			}
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			if(!(StringUtils.isEmpty(startDate))) {
				if(!(StringUtils.isEmpty(endDate))) {
					ps.setString(1, startDate);
					ps.setString(2, endDate + "23:59:59");
				}else{
					ps.setString(1, startDate);
					ps.setString(2, "2100-01-01");
				}
			}else{
				if(!(StringUtils.isEmpty(endDate))) {
					ps.setString(1, "2010-01-01");
					ps.setString(2, endDate + "23:59:59");

				}else{
					ps.setString(1, "2010-01-01");
					ps.setString(2, "2100-01-01");
				}
			}
			if(!(StringUtils.isEmpty(category))) {
				ps.setString(3, "%" + category + "%");
			}

			ResultSet rs = ps.executeQuery();
			List<UserPost> ret = toUserMessageList(rs);
			return ret;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	private List<UserPost> toUserMessageList(ResultSet rs)
			throws SQLException {

		List<UserPost> ret = new ArrayList<UserPost>();
		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				String subject = rs.getString("subject");
				String mainPost = rs.getString("main_post");
				String category = rs.getString("category");
				String createdDate = rs.getString("created_date");
				int userId = rs.getInt("user_id");

				createdDate = createdDate.substring(0, createdDate.length() - 2);

				UserPost post = new UserPost();
				post.setId(id);
				post.setSubject(subject);
				post.setName(name);
				post.setMainPost(mainPost);
				post.setCategory(category);
				post.setUserId(userId);
				post.setCreated_date(createdDate);

				ret.add(post);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}
