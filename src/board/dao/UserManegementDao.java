package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.Member;
import board.exception.SQLRuntimeException;

public class UserManegementDao {

    public List<Member> getMember(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
			sql.append("select users.id as id");
			sql.append(", users.login_id as login_id");
			sql.append(", users.name as name");
			sql.append(", users.branch as branch");
			sql.append(", users.password as password");
			sql.append(", users.position as position");
			sql.append(", users.is_stoped as isStop");
			sql.append(", users.created_date as created_date");
			sql.append(", users.updated_date as udated_date");
			sql.append(", users.login_id as login_id");
			sql.append(", branches.id as branch_id");
			sql.append(", branches.branches_name as branch_name");
			sql.append(", positions.id as position_id");
			sql.append(", positions.position_name as position_name");
			sql.append(" from users");
			sql.append(" left join");
			sql.append(" branches ");
			sql.append(" on");
			sql.append(" users.branch = branches.id");
			sql.append(" left join positions");
			sql.append(" on");
			sql.append(" users.position = positions.id;");

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Member> ret = toMemberList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<Member> toMemberList(ResultSet rs)
            throws SQLException {

        List<Member> ret = new ArrayList<Member>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                int branch = rs.getInt("branch");
                int position = rs.getInt("position");
                int branchId = rs.getInt("branch_id");
                int isStop = rs.getInt("isStop");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");
                Timestamp createdDate = rs.getTimestamp("created_date");

                Member member = new Member();
                member.setId(id);
                member.setLoginId(loginId);
                member.setName(name);
                member.setBranch(branch);
                member.setPosition(position);
                member.setBranchId(branchId);
                member.setIsStop(isStop);
                member.setBranchName(branchName);
                member.setPositionName(positionName);
                member.setCreatedDate(createdDate);

                ret.add(member);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}