package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.exception.SQLRuntimeException;

public class PostDeleteDao {
	public void delete(Connection connection, int postId) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("delete from posts where id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, postId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}