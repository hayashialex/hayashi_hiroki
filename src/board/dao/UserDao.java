package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.User;
import board.exception.SQLRuntimeException;


public class UserDao {

	public void insert(Connection connection, User user) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", name");
			sql.append(", password");
			sql.append(", branch");
			sql.append(", position");
			sql.append(", is_stoped");
			sql.append(") VALUES (");
			sql.append("?"); // roginId
			sql.append(", ?"); // name
			sql.append(", ?"); // password
			sql.append(", ?"); // branch
			sql.append(", ?"); // position
			sql.append(", ?"); // isStop
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLoginId());
			ps.setString(2, user.getName());
			ps.setString(3, user.getPassword());
			ps.setInt(4, user.getBranch());
			ps.setInt(5, user.getPosition());
			ps.setInt(6, user.getIsStop());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
		public User getUser(Connection connection, String loginId,
		        String password) {

		    PreparedStatement ps = null;
		    try {
		        String sql = "SELECT * FROM users WHERE login_id = ? AND password = ?";

		        ps = connection.prepareStatement(sql);
		        ps.setString(1, loginId);
		        ps.setString(2, password);

		        ResultSet rs = ps.executeQuery();
		        List<User> userList = toUserList(rs);
		        if (userList.isEmpty() == true) {
		            return null;
		        } else if (2 <= userList.size()) {
		            throw new IllegalStateException("2 <= userList.size()");
		        } else {
		            return userList.get(0);
		        }
		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(ps);
		    }
		}

		private List<User> toUserList(ResultSet rs) throws SQLException {

		    List<User> ret = new ArrayList<User>();
		    try {
		        while (rs.next()) {
		            int id = rs.getInt("id");
		            String name = rs.getString("name");
		            String password = rs.getString("password");
		            int position = rs.getInt("position");
		            int branch = rs.getInt("branch");
		            int isStop = rs.getInt("is_stoped");
		            Timestamp createdDate = rs.getTimestamp("created_date");
		            Timestamp updatedDate = rs.getTimestamp("updated_date");

		            User user = new User();
		            user.setId(id);
		            user.setName(name);
		            user.setPassword(password);
		            user.setPosition(position);
		            user.setBranch(branch);
		            user.setIsStop(isStop);
		            user.setCreatedDate(createdDate);
		            user.setUpdatedDate(updatedDate);

		            ret.add(user);
		        }
		        return ret;
		    } finally {
		        close(rs);
		    }
		}
	}
