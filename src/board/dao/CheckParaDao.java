package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import board.beans.User;
import board.exception.SQLRuntimeException;

public class CheckParaDao {
	public boolean CheckPara(Connection connection, int intCheckParaId) {

		   PreparedStatement ps = null;
		    try {
		        String sql = "SELECT * FROM users WHERE login_id = ?";

		        ps = connection.prepareStatement(sql);
		        ps.setInt(1, intCheckParaId);

		        ResultSet rs = ps.executeQuery();
		        List<User> userList = toUserList(rs);
		        if (userList.isEmpty() == true) {
		            return true;
		        } else {
		            return false;
		        }
		    } catch (SQLException e) {
		        throw new SQLRuntimeException(e);
		    } finally {
		        close(ps);
		    }
		}
	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String name = rs.getString("name");
	            String password = rs.getString("password");
	            int position = rs.getInt("position");
	            int branch = rs.getInt("branch");
	            int isStop = rs.getInt("is_stoped");
	            Timestamp createdDate = rs.getTimestamp("created_date");
	            Timestamp updatedDate = rs.getTimestamp("updated_date");

	            User user = new User();
	            user.setId(id);
	            user.setName(name);
	            user.setPassword(password);
	            user.setPosition(position);
	            user.setBranch(branch);
	            user.setIsStop(isStop);
	            user.setCreatedDate(createdDate);
	            user.setUpdatedDate(updatedDate);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}
}
