package board.dao;

import static board.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import board.exception.SQLRuntimeException;

public class CommentDeleteDao {
	public void delete(Connection connection, int commentId) {

		PreparedStatement ps = null;
		try{
			StringBuilder sql = new StringBuilder();
			sql.append("delete from comments where id = ?;");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, commentId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}