package board.beans;

import java.io.Serializable;

public class UserComment implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String main_comment;
	private String createdDate;
	private int userId;
	private int messageId;
	private String name;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getMessageId() {
		return messageId;
	}
	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getMain_comment() {
		return main_comment;
	}
	public void setMain_comment(String main_comment) {
		this.main_comment = main_comment;
	}
}

