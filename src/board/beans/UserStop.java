package board.beans;

import java.io.Serializable;

public class UserStop implements Serializable {
	private static final long serialVersionUID = 1L;

private int userId;
private int isStoped;
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public int getIsStoped() {
	return isStoped;
}
public void setIsStoped(int isStoped) {
	this.isStoped = isStoped;
}
}

