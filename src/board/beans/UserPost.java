package board.beans;

import java.io.Serializable;

public class UserPost implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String name;
    private String subject;
    private String mainPost;
    private int userId;
    private String category;
    private String created_date;
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getMainPost() {
		return mainPost;
	}
	public void setMainPost(String mainPost) {
		this.mainPost = mainPost;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCreated_date() {
		return created_date;
	}
	public void setCreated_date(String created_date) {
		this.created_date = created_date;
	}
}