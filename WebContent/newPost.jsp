<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="css/newPost.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>投稿画面</title>
</head>
<body>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

		<div class="formArea">
				<form action="newMessage" method="post">
					<br />
					<label for="subject">件名(30文字以内)</label> <br />
					<input type="text" name="subject" size="30" maxlength="40" value="${subject}" class="subject"></input>
					<br />
					<label for="mainPost">本文(1000文字以内)</label> <br />
					<textarea name="mainPost" cols="75" rows="12" class="mainPost">${mainPost}</textarea>
					<br />
					<label for="category">カテゴリ(10文字以内)</label> <br />
				<input type="text" name="category" size="10" maxlength="20" value="${category}" class="category"></input>
					<br /> <input type="submit" value="投稿">
				<input type="hidden" name="loginUser" value="${editUser.id}">
				</form>
			<a href="./">ホーム画面に戻る</a>
		</div>
		<div class="copyright">Copyright(c)H.Hayashi</div>
</body>
</html>