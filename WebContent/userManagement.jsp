<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/userManagement.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー管理画面</title>
</head>
<body>
<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		</div>
	<a href="signup">ユーザー新規登録</a>
    <a href="./">ホーム画面に戻る</a>
	<div class="members">
	<table class="table" border="1">
	<tr>
	<th>ログインID</th><th>名前</th><th>支店名</th><th>役職名</th><th>停止/再開</th><th>編集</th>
	</tr>
		<c:forEach items="${members}" var="member">
			<div class="member">
				<div class="account-name">
				<tr>
					<td><span class="loginId"><c:out value="${member.loginId}" /></span> </td>
					<td><span	class="name"><c:out value="${member.name}" /></span> </td>
					<td><span class="branchName"><c:out value="${member.branchName}" /></span></td>
					<td><span class="position"><c:out value="${member.positionName}" /></span></td>

					<script type="text/javascript">
 					<!--
 					function stopCheck(){
 						if(window.confirm('ユーザーを停止させますか？')){
 							return true;
 						}else{
 							return false;
 						}
 					}
 					function returnCheck(){
 						if(window.confirm('ユーザーを復活させますか？')){
 							return true;
 						}else{
 							return false;
 						}
 					}
					// -->
					</script>

					<td>
					<c:if test="${loginUser.id != member.id}" >
					<c:if test="${member.isStop == 0}" >

						<form method="post" action="userStop" onSubmit="return stopCheck()">
							<input type="hidden" name="UserId" value="${member.id}">
						<input type="submit" value="停止">
						</form>
					</c:if>
					<c:if test="${member.isStop == 1}">
						<form method="post" action="userReturn" onSubmit="return returnCheck()">
							<input type="hidden" name="ReturnUserId" value="${member.id}">
							<input type="submit" value="復活">
						</form>
					</c:if>
					</c:if>
					<c:if test="${loginUser.id == member.id}" >
					<div>ログイン中</div>
					</c:if>
					</td>
					<td>
					<form method="get" action="editUserSevlet">
						<input type="hidden" name="EditUserId" value="${member.id}">
						<input type="submit" value="ユーザー編集画面">
					</form>
					</td>
				</tr>
				</div>
				<div class="date">
					<fmt:formatDate value="${message.created_date}"
						pattern="yyyy/MM/dd HH:mm:ss" />
				</div>
			</div>
		</c:forEach>
		</table>
	</div>
	<div class="copyright">Copyright(c)H.Hayashi</div>
</body>
</html>
