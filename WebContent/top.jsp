<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/top.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ホーム画面</title>
</head>
<body>

    <c:if test="${ not empty errorMessages }">
        <div class="errorMessages">
            <ul>
                <c:forEach items="${errorMessages}" var="message">
                    <li><c:out value="${message}" />
                </c:forEach>
            </ul>
        </div>
        <c:remove var="errorMessages" scope="session"/>
    </c:if>
	<a class="logout" href="logout">ログアウト</a>

		<script type="text/javascript">
 					<!--
 					function deletePost(){
 						if(window.confirm('投稿を削除しますか？')){
 							return true;
 						}else{
 							return false;
 						}
 					}
					// -->
					</script>

	<div class="messages">
		<c:forEach items="${posts}" var="post">
			<div class="message">
				<div class="account-name">
					<span class="subject">件名:<c:out value="${post.subject}" /></span>
				</div>

				<div class="text"><pre class="mainPost"><c:out value="${post.mainPost}" /></pre>
				<div class="name">投稿者:<c:out value="${post.name}" /></div>
				</div>
				<div class="date"><c:out value="${post.created_date}" /></div>
				<div class="category">カテゴリ:<c:out value="${post.category}" /></div>
				<c:if test="${post.userId == loginUser.id}" var = "trueComment">
				<form method="post" action="postDelete" onSubmit="return deletePost()">
						<input type="hidden" name="postId" value="${post.id}">
						<input type="submit" value="投稿の削除">
				</form>
				</c:if>
					<script type="text/javascript">
 					<!--
 					function deleteCheck(){
 						if(window.confirm('コメントを削除しますか？')){
 							return true;
 						}else{
 							return false;
 						}
 					}
					// -->
					</script>
				<div class="main_comment">
				<c:forEach items="${comments}" var="comment">
				<c:if test="${post.id == comment.messageId}" var = "trueComment">
				<c:if test="${trueComment}" >
					<div class="mainComment"><pre class="preComment"><c:out value="${comment.main_comment}" /></pre>
					by.<c:out value="${comment.name}" />
					</div>
				<c:if test="${comment.userId == loginUser.id}" var = "trueComment">
					<form method="post" action="commentDelete" onSubmit="return deleteCheck()">
						<input type="hidden" name="commentId" value="${comment.id}">
						<input type="submit" value="コメントの削除">
					</form>
					</c:if>
				</c:if>
				</c:if>
				</c:forEach>
				</div>
			</div>
			<br />
			<div class="commentPost">
			<form action="comment" method="post">
				<input type="hidden" name="postId" value="${post.id}">
				<textarea name="mainComment" cols="50" rows="5" class="commentPost"><c:if test="${post.id == postIdNumber}">${mainComment}</c:if></textarea>
				<br /> <input type="submit" value="コメント投稿">
			</form>
			</div>
		</c:forEach>
				<c:remove var="mainComment" scope="session"/>
				<div class="sidebar">
				<form action="index.jsp">
				<label for="categorySerch">カテゴリー</label><br /><input type = "text" name="category" id="category" value="${category}" /><br />
				<label for="dateSerch">日時検索</label><br />
				<input type="date" name="startDate" value="${startDate}"/>~<input type="date" name="endDate" value="${endDate}"/>
				<input type="submit" value="検索">
				</form>
				<br />
				<a class="Post" href="./">リセット</a>
				<br />
				<br />
				<c:if test="${loginUser.branch == 1}">
				<a class="management" href="Member">ユーザー管理画面</a>
				</c:if>
				<br />
				<a class="Post" href="newMessage">投稿画面</a>
				</div>

	</div>
</body>
</html>