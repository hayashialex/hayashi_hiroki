<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="./css/signup.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<a href="Member">ユーザー管理画面に戻る</a>
		<div class="signup">
		<form action="signup" method="post">
			<br /> <label for="roginId">ログインID</label> <br /> <input name="roginId" value ="${roginId}"
			id="roginId" /><br />
			<label for="name">名前</label><br /><input name="name" id="name" value="${name}" />
			 <br /><label for="password">パスワード</label><br /><input name="password" type="password" id="password" />
			 <br /><label for="passwordSecond">確認用パスワード</label><br />
			 <input name="passwordSecond" type="password" id="passwordSecond" /><br />

				<label for="branch">支店</label><br>
				<select name="branch">
				<c:forEach items="${branch}" var="branch">
				<c:if test="${branchId == branch.id}" var="branchFlg" />
				<c:if test="${branchFlg}" >
				<option  value="${branch.id}" selected>${branch.branchName}</option>
				</c:if>
				<c:if test="${!branchFlg}" >
				<option value="${branch.id}">${branch.branchName}</option>
				</c:if>
				</c:forEach>
				</select>
			<br />
			<label for="position">部署・役職</label><br>
				<select name="position">
				<c:forEach items="${position}" var="position">
				<c:if test="${positionId == position.id}" var="positionFlg" />
				<c:if test="${positionFlg}" >
				<option value="${position.id}" selected>${position.positionName}</option>
				</c:if>
				<c:if test="${!positionFlg}" >
				<option value="${position.id}" >${position.positionName}</option>
				</c:if>
				</c:forEach>
			</select>

			 <br /> <input type="submit" value="登録" /> <br />
			</form>
		<div class="copyright">Copyright(c)H.Hayashi</div>
	</div>
	</div>
</body>
</html>